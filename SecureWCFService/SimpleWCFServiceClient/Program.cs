﻿using System;
using System.ServiceModel;



 
namespace HelloWorldClient
{
    class Program
    {
        static string m_ServiceAddress = "localhost";
        static int m_PortNumber = 5432;
        static string m_ServiceEndpoint = "HelloWorldService";
        static string m_ServiceUrl = "https://localhost:4321/HelloWorldService";

        static void Main(string[] args)
        {
            for (int iArg = 0; iArg < args.Length; ++iArg)
            {
                System.Diagnostics.Debug.WriteLine(args[iArg]);
                string arg = args[iArg].ToLower();
                if (arg == "-p" && ++iArg < arg.Length)
                {
                    int.TryParse(args[iArg], out m_PortNumber);
                }
            }

            m_ServiceUrl = "https://" + m_ServiceAddress + ":" + m_PortNumber.ToString() + "/" + m_ServiceEndpoint;
            Console.WriteLine(m_ServiceUrl);
            Console.WriteLine("Press enter when the service is opened.");
            Console.ReadLine();
 
            BasicHttpBinding binding = new BasicHttpBinding();
            binding.Security.Mode = BasicHttpSecurityMode.Transport;

            ChannelFactory<IHelloWorld> factory = new ChannelFactory<IHelloWorld>(binding, m_ServiceUrl);
            IHelloWorld client = factory.CreateChannel();
            Console.WriteLine("Invoking HelloWorld on the service.");
            client.HelloWorld();
            Console.WriteLine("Press enter to quit.");
            Console.ReadLine();
        }
    }
 
    [ServiceContract]
    public interface IHelloWorld
    {
        [OperationContract]
        void HelloWorld();
    }
}