﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace CertReg
{

    class Program
    {
        static string m_CertIssuerName = "CN=Z Dev";
        static string m_CertAddress = "0.0.0.0";
        static string m_CertDescriptor = "localhost";
        static int m_PortNumber = 5432;

        static void Main(string[] args)
        {
            try
            {
                for (int iArg = 0; iArg < args.Length; ++iArg)
                {
                    System.Diagnostics.Debug.WriteLine(args[iArg]);
                    string arg = args[iArg].ToLower();
                    if (arg == "-p" && ++iArg < arg.Length)
                    {
                        int.TryParse(args[iArg], out m_PortNumber);
                    }
                    if (arg == "-i" && ++iArg < args.Length)
                    {
                        m_CertIssuerName = "CN=" + args[iArg];
                    }
                }

                createAndRegisterCerts();
            }
            catch (Exception x)
            {
                string sErr = x.Message;
            }
        }

        static void createAndRegisterCerts()
        {
            try
            {
                string addressAndPort = m_CertDescriptor + ":" + m_PortNumber.ToString();
                string certName = "CN=" + m_CertDescriptor;

                var applicationId = ((GuidAttribute)typeof(Program).Assembly.GetCustomAttributes(typeof(GuidAttribute), true)[0]).Value;

                CertGen.Reg certReg = new CertGen.Reg(m_CertIssuerName, applicationId, m_CertAddress, m_CertDescriptor, m_PortNumber);
                certReg.CreateAndRegisterCerts();
            }
            catch (Exception x)
            {
                string sErr = x.Message;
            }
        }
        static string executeCommand(string action)
        {
            StringBuilder stringBuilder = new StringBuilder();
            using (Process process = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    WindowStyle = ProcessWindowStyle.Normal,
                    FileName = "cmd.exe",
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    Arguments = "/c " + action
                }
            })
            {
                Console.WriteLine("Executing Command:");
                Console.WriteLine(action);
                process.Start();

                while (!process.StandardOutput.EndOfStream)
                {
                    stringBuilder.AppendLine(process.StandardOutput.ReadLine());
                }
                int exitCode = process.ExitCode;
                process.Close();

            }

            return stringBuilder.ToString();
        }       
    }

    
}
