﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace CertGen
{
    public class Reg
    {
        string m_CertIssuerName = "CN=Z Dev";
        string m_CertAddress = "0.0.0.0";
        string m_CertDescriptor = "localhost";
        int m_PortNumber = 5432;
        string m_AppId;

        public Reg(string certIssuerName, string appId, string certAddress, string certDescriptor, int portNumber)
        {
            m_CertIssuerName = certIssuerName;
            m_AppId = appId;
            m_CertAddress = certAddress;
            m_CertDescriptor = certDescriptor;
            m_PortNumber = portNumber;
        }
        public void CreateAndRegisterCerts()
        {
            try
            {
                string addressAndPort = m_CertDescriptor + ":" + m_PortNumber.ToString();
                string certName = "CN=" + m_CertDescriptor;

                //var applicationId = ((GuidAttribute)typeof(Program).Assembly.GetCustomAttributes(typeof(GuidAttribute), true)[0]).Value;

                CertGen.Gen keyGen = new CertGen.Gen();
                var caPrivKey = keyGen.GenerateCACertificate(m_CertIssuerName);
                var cert = keyGen.GenerateSelfSignedCertificate(certName, m_CertIssuerName, caPrivKey);
                keyGen.AddCertToStore(cert, StoreName.My, StoreLocation.LocalMachine);

                //Reg certReg = new Reg(m_CertIssuerName, applicationId, cert.Thumbprint, m_CertAddress, m_CertDescriptor, m_PortNumber);

                Console.WriteLine("Removing existing instance..");
                Console.WriteLine(executeCommand("netsh http delete sslcert ipport=" + m_CertAddress + ":" + m_PortNumber.ToString()));

                string sCmd = @"netsh http add sslcert ipport=" + m_CertAddress + ":" + m_PortNumber.ToString();
                sCmd += @" certhash=" + cert.Thumbprint;
                sCmd += @"  appid={" + m_AppId + "}";
                Console.WriteLine(executeCommand(sCmd));

            }
            catch (Exception x)
            {
                string sErr = x.Message;
            }
        }
        string executeCommand(string action)
        {
            StringBuilder stringBuilder = new StringBuilder();
            using (Process process = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    WindowStyle = ProcessWindowStyle.Normal,
                    FileName = "cmd.exe",
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    Arguments = "/c " + action
                }
            })
            {
                Console.WriteLine("Executing Command:");
                Console.WriteLine(action);
                process.Start();

                while (!process.StandardOutput.EndOfStream)
                {
                    stringBuilder.AppendLine(process.StandardOutput.ReadLine());
                }
                int exitCode = process.ExitCode;
                process.Close();

            }

            return stringBuilder.ToString();
        }
    }
}
