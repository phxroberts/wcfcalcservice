﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using WCFCalcService;

namespace WindowsServiceHost
{
    public partial class CalculatorServiceHost : ServiceBase
    {
        ServiceHost host = new ServiceHost(typeof(Calculator));

        public CalculatorServiceHost()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            host.Open();
        }

        protected override void OnStop()
        {
            host.Close();
        }
    }
}
