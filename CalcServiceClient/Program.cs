﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using WCFCalcService;

namespace CalcServiceClient
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("Press enter when the service is opened.");
                Console.ReadLine();

                string address = "http://localhost:8080/calcservice/main";
                BasicHttpBinding binding = new BasicHttpBinding();
                binding.Security.Mode = BasicHttpSecurityMode.None;

                ChannelFactory<ICalculator> factory = new ChannelFactory<ICalculator>(binding, address);
                ICalculator client = factory.CreateChannel();
                Console.WriteLine("Invoking Operation on the service.");
                var retval = client.GetData(44);
                var result = client.Add(22, 6);
                Console.WriteLine("Press enter to quit.");
                Console.ReadLine();
            }
            catch (Exception x)
            {
                string sErr = x.Message;
            }
        }
    }
}
