﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Text;
using System.Threading.Tasks;
using WCFCalcService;

namespace CalcServiceHost
{
    class Program
    {
        static ServiceHost createConfigBasedService(bool bAddBaseAddress=false)
        {
            ServiceHost host = null;
            if (bAddBaseAddress)
            {
                host = new ServiceHost(typeof(Calculator), new Uri[] {
                    new Uri("http://localhost:8080/calcservice")
                });
            }
            else
            {
                host = new ServiceHost(typeof(Calculator));
            }
            return host;
        }
        static ServiceHost createService()
        {
            string address = "http://localhost:8080/calcservice";
            BasicHttpBinding binding = new BasicHttpBinding();
            ServiceHost host = new ServiceHost(typeof(Calculator), new Uri[] {
                    new Uri("http://localhost:8080/calcservice")
                });
            ServiceEndpoint SEP = host.AddServiceEndpoint(typeof(ICalculator), binding, address);
            return host;
        }
        static void addServiceBehaviors(ServiceHost host)
        {
            ServiceMetadataBehavior smb = new ServiceMetadataBehavior();
            var metdataExporter = smb.MetadataExporter;
            smb.HttpGetEnabled = true;
            // smb.HttpGetUrl = host.Description.Endpoints[0].Address.Uri;
            smb.MetadataExporter.PolicyVersion = PolicyVersion.Policy15;
            host.Description.Behaviors.Add(smb);
            //host.AddServiceEndpoint(
            //  ServiceMetadataBehavior.MexContractName,
            //  MetadataExchangeBindings.CreateMexHttpBinding(),
            //  "mex"
            //);
        }
        static void Main(string[] args)
        {
            try {

                
                //ServiceHost host = createService();
                ServiceHost host = createConfigBasedService();

                int baseAddressCount = host.BaseAddresses.Count;
                addServiceBehaviors(host);



                int endpointCount = host.Description.Endpoints.Count;
                int behaviorsCount = host.Description.Behaviors.Count;

                host.Open();

                

                Console.WriteLine("Service is up and running");

                Console.ReadLine();

                host.Close();
            }
            catch (Exception x)
            {
                System.Diagnostics.Debug.WriteLine(x.Message);
            }
        }
    }
}
